package richtercloud.datanucleus.basic.hash.map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author richter
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("richtercloud_datanucleus-basic-hash-map_jar_1.0-SNAPSHOTPU");
        EntityManager em = emf.createEntityManager();
        MyEntity myEntity = new MyEntity();
        em.getTransaction().begin();
        em.persist(myEntity);
        em.flush();
        em.getTransaction().commit();
        em.close();
    }
    
}
